package edu.sjsu.android.mortgagecalculator;

import java.lang.Math;
public class Calculation {



    public static float zeroInterest(float P, int N, float T) {
        float result = Math.round(((P/N) + T) * 100);
        return result/100;
    }


    public static float interestCalc(float P, float J, int N, float T) {
        float bottom = (float) Math.pow((1 + J), -N);
        float result = Math.round((P * (J / (1 - bottom)) + T) * 100);
        return result/100;
    }
}