package edu.sjsu.android.mortgagecalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private TextView interest;
    private EditText input;
    private SeekBar seekbar;
    private TextView finalCalc;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        interest = (TextView) findViewById(R.id.textView2);
        input = (EditText) findViewById(R.id.editText);
        seekbar = (SeekBar) findViewById(R.id.seekBar);
        finalCalc = (TextView) findViewById(R.id.textView);

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            //to change the number to decimal, check hw doc and set the textview above the seekbar
            //to display the value chosen
            public void onProgressChanged(SeekBar seekBar, int number, boolean userInput) {
                float decimal = (float)(number)/10;
                interest.setText(decimal + "/20.0%");
            }

            @Override
            //look at slides
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    public void onClick(View view) {

        //for the different conditions of selected time
        //and the checkbox
        switch (view.getId()) {
            case R.id.button:
                RadioButton fifteen =
                        (RadioButton) findViewById(R.id.radioButton);
                RadioButton twenty =
                        (RadioButton) findViewById(R.id.radioButton2);
                RadioButton thirty =
                        (RadioButton) findViewById(R.id.radioButton3);
                CheckBox taxesAndInsurance=
                        (CheckBox) findViewById(R.id.checkBox);

                if (input.getText().length() == 0) {
                    Toast.makeText(this, "Please enter a valid number",
                            Toast.LENGTH_LONG).show();
                    return;
                }
                float inputNum = Float.parseFloat(input.getText().toString());
                float tax = 0;
                float monthlyInterest = 0;

                //go back to improve code, way too many if statements
                //remember when doing thirty wont need to type it cause went through all!
                if(seekbar.getProgress() != 0)
                {
                    monthlyInterest = (float) (seekbar.getProgress())/12000;
                }
                if (taxesAndInsurance.isChecked()) {
                    tax = (float) (inputNum * 0.001);
                }
                if (fifteen.isChecked()) {
                    if(monthlyInterest == 0){
                        finalCalc.setText("$" + String
                                .valueOf(Calculation.zeroInterest(inputNum, 15*12, tax)));
                    }
                    else {
                        finalCalc.setText("$" + String
                                .valueOf(Calculation.interestCalc(inputNum, monthlyInterest,15*12, tax)));
                    }
                }
                else if (twenty.isChecked()){
                    if(monthlyInterest == 0){
                        finalCalc.setText("$" + String
                                .valueOf(Calculation.zeroInterest(inputNum, 20*12, tax)));
                    }
                    else {
                        finalCalc.setText("$" + String
                                .valueOf(Calculation.interestCalc(inputNum, monthlyInterest, 20*12, tax)));
                    }
                }
                else {
                    if(monthlyInterest == 0){
                        finalCalc.setText("$" + String
                                .valueOf(Calculation.zeroInterest(inputNum, 30*12, tax)));
                    }
                    else {
                        finalCalc.setText("$" + String
                                .valueOf(Calculation.interestCalc(inputNum, monthlyInterest, 30*12, tax)));
                    }
                }
                break;
        }
    }
}
